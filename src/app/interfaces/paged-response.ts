export interface PagedResponse {
    data: any;
    total: number;
    total_pages: number;
}
