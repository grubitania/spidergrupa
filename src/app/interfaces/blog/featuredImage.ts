export interface FeaturedImage {
    alt_text: string,
    caption: string,
    description: string,
    media_details: {
        width: number,
        height: number,
        sizes: any
    },
    source_url: string

}
