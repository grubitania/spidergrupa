import { Category } from "../category/category";

export interface Blog {
    id: number;
    title: {
        rendered: string
    };
    content: {
        rendered: string
    };
    categories: number[];
    excerpt: {
        rendered: string
    };
    author: number;
    acf: any;
    better_featured_image: any;
    slug: string;
    readingTime: number;
    date: string;
    modified: string;
    yoast_meta?: {
        yoast_wpseo_canonical: string,
        yoast_wpseo_focuskw: string,
        yoast_wpseo_linkdex: string,
        yoast_wpseo_meta_robots_adv: string,
        yoast_wpseo_meta_robots_nofollow: string,
        yoast_wpseo_meta_robots_noindex: string,
        yoast_wpseo_metadesc: string,
        yoast_wpseo_metakeywords: string,
        yoast_wpseo_opengraph_description: string,
        yoast_wpseo_opengraph_image: string,
        yoast_wpseo_opengraph_title: string,
        yoast_wpseo_redirect: string,
        yoast_wpseo_title: string,
        yoast_wpseo_twitter_description: string,
        yoast_wpseo_twitter_image: string,
        yoast_wpseo_twitter_title: string
    };
}
