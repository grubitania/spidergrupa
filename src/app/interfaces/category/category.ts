export interface Category {
    acf?: any,
    count: number,
    name: string,
    slug: string,
    id: number,
    taxonomy: string
}
