import { Component } from '@angular/core';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.sass']
})
export class AppComponent {
    title = 'app';

    constructor(
        translate: TranslateService,
        private router: Router
    ) {
        translate.setDefaultLang('hr');
        const lang = localStorage.getItem("lang");

        if (lang){
            translate.use(lang);
        }

    }
}
