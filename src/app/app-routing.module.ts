import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { NewsComponent } from './components/news/news.component';
import { SingleComponent } from './components/single/single.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { GallerySingleComponent } from './components/gallery-single/gallery-single.component';
import { PageComponent } from './components/page/page.component';

const routes: Routes = [
    { path: 'about', component: AboutComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'news', component: NewsComponent },
    { path: "news/page/:pageNum", component: NewsComponent },
    { path: "news/category/:categorySlug", component: NewsComponent },
    { path: "news/category/:categorySlug/page/:pageNum", component: NewsComponent },
    { path: 'news/:slug', component: SingleComponent },
    { path: 'gallery', component: GalleryComponent },
    { path: 'gallery/:slug', component: GallerySingleComponent },
    { path: 'page/:slug', component: PageComponent },
    { path: '', component: HomeComponent }
];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}
