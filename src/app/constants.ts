import { environment } from "../environments/environment";

export class Constants {
    public static API_URL: string = environment.API_URL;
    public static ACF_URL: string = environment.ACF_URL;
    public static DEFAULT_IMAGE: string = "/assets/images/default_blog.jpg";

    public static getApiUrl(): string {
        return this.API_URL;
    }
    public static getAcfUrl(): string {
        return this.ACF_URL;
    }
}
