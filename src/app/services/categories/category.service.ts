import { Injectable } from '@angular/core';
import { Constants } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { Category } from '../../interfaces/category/category';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class CategoryService {

    public static ENDPOINT_ALL_CATEGORIES: string = 'categories';
    public static ENDPOINT_CATEGORIES: string = 'categories?include=';
    public static ENDPOINT_GET_CATEGORIES_BY_SLUG: string = 'categories?slug=';

    categories: Array<Category>;

    constructor(private http: HttpClient) { }

    public getCategoriesBySlug(postCategories: Array<string>): Observable<Array<Category>> {

        return new Observable<Array<Category>>(observer => {

            this.loadCategoriesBySlug(postCategories).subscribe(
                response => {
                    // console.log(response);
                    if (response) {
                        this.categories = response;
                        observer.next(response);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadCategoriesBySlug(postCategories: Array<string>): Observable<Array<Category>> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        CategoryService.ENDPOINT_GET_CATEGORIES_BY_SLUG + postCategories.join(',') + "&lang=" + lang;

        return this.http.get<Array<Category>>(url);
    }

    public getCategories(postCategories: Array<number>): Observable<Array<Category>> {

        return new Observable<Array<Category>>(observer => {

            this.loadCategories(postCategories).subscribe(
                response => {
                    if (response) {
                        this.categories = response;
                        observer.next(response);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadCategories(postCategories: Array<number>): Observable<Array<Category>> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        CategoryService.ENDPOINT_CATEGORIES + postCategories.join(',') + "&lang=" + lang;

        return this.http.get<Array<Category>>(url);
    }

    public getAllCategories(): Observable<Array<Category>> {

        return new Observable<Array<Category>>(observer => {

            this.loadAllCategories().subscribe(
                response => {
                    // console.log(response);
                    if (response) {
                        this.categories = response;
                        observer.next(response);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadAllCategories(): Observable<Array<Category>> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        CategoryService.ENDPOINT_ALL_CATEGORIES + "?per_page=100" + "&lang=" + lang;

        return this.http.get<Array<Category>>(url);
    }

}
