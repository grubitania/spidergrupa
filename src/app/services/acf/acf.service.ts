import { Injectable } from '@angular/core';
import { Constants } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';


@Injectable()
export class AcfService {

    public static ENDPOINT_PAGE_ACF: string = 'pages';

    constructor(private http: HttpClient) { }

    public getACF(pageId: any): Observable<any> {

        return new Observable<any>(observer => {

            this.loadACF(pageId as any).subscribe(
                response => {
                    // console.log(response);
                    if (response) {
                        observer.next(response['acf']);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadACF(pageId: any): Observable<any> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getAcfUrl() +
        AcfService.ENDPOINT_PAGE_ACF + '/' + pageId + "&lang=" + lang;

        return this.http.get<any>(url);
    }

    public getACFcpt(cpt: string): Observable<any> {
        return new Observable<any>(observer => {
            this.loadACFcpt(cpt as string).subscribe(
                response => {
                    console.log(response);
                    if (response) {
                        observer.next(response);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadACFcpt(cpt: any): Observable<any> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getAcfUrl() + cpt + "&lang=" + lang;
        return this.http.get<any>(url);
    }

}
