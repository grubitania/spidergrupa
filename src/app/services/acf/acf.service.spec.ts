/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { AcfService } from './acf.service';

describe('Service: Acf', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AcfService]
    });
  });

  it('should ...', inject([AcfService], (service: AcfService) => {
    expect(service).toBeTruthy();
  }));
});
