import { Injectable } from '@angular/core';
import { Constants } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { ConnectableObservable } from 'rxjs/internal/observable/ConnectableObservable';

import { publish } from 'rxjs/operators';


@Injectable()
export class ContactService {

    public static ENDPOINT_SEND_CONTACT_FORM: string = 'sendContact';

    constructor(private http: HttpClient) { }

    public sendContact(data: any): ConnectableObservable<Response>{
        let req;
        delete data['gdpr'];

        if (data){
            req = this.http.post<Response>(
                Constants.getApiUrl() + ContactService.ENDPOINT_SEND_CONTACT_FORM, data).pipe(publish());
        }
        return req;
    }
}
