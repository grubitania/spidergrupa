/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ContactThankYouService } from './contact-thank-you.service';

describe('Service: ContactThankYou', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ContactThankYouService]
    });
  });

  it('should ...', inject([ContactThankYouService], (service: ContactThankYouService) => {
    expect(service).toBeTruthy();
  }));
});
