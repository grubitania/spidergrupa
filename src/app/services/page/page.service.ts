import { Injectable } from '@angular/core';
import { Blog } from '../../interfaces/blog/blog';
import { Observable } from 'rxjs';
import { Constants } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class PageService {

    public static ENDPOINT_PAGE: string = "pages?slug=";

    page: Blog;

    constructor(
        private http: HttpClient
    ) {}

    public getPage(slug: string): Observable<Blog> {

        return new Observable<Blog>(observer => {

            this.loadPage(slug as string).subscribe(
                response => {
                    // console.log(response);
                    if (response) {
                        this.page = response[0];
                        observer.next(this.page);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadPage(slug: string): Observable<Blog> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        PageService.ENDPOINT_PAGE + slug + "&lang=" + lang;

        return this.http.get<Blog>(url);
    }

}
