import { Injectable } from '@angular/core';
import { Blog } from '../../interfaces/blog/blog';
import { FeaturedImage } from '../../interfaces/blog/featuredImage';
import { Constants } from '../../constants';
import { HttpClient } from '@angular/common/http';
import { PagedResponse } from '../../interfaces/paged-response';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class BlogService {

    public static ENDPOINT_POSTS: string = 'posts';
    public static ENDPOINT_POST: string = 'posts?slug=';
    public static ENDPOINT_CATEGORY_POSTS: string = "posts/categories";

    posts: Array<Blog>;
    post: Blog;
    pagedResponse: PagedResponse;

    constructor(
        private http: HttpClient
    ) { }

    public getPost(slug: string): Observable<Blog> {

        return new Observable<Blog>(observer => {

            this.loadPost(slug as string).subscribe(
                response => {
                    // console.log(response);
                    if (response) {
                        this.post = response[0];
                        this.post['readingTime'] = this.prefix_estimated_reading_time(this.post.content.rendered);
                        observer.next(response[0]);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadPost(slug: string): Observable<Blog> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        BlogService.ENDPOINT_POST + slug + "&lang=" + lang;

        return this.http.get<Blog>(url);
    }

    public getPosts(limit, page: number = 1): Observable<PagedResponse> {

        return new Observable<PagedResponse>(observer => {

            this.loadPosts(limit, page).subscribe(
                response => {
                    const total_pages = response.headers.get("X-WP-TotalPages");
                    const total = response.headers.get("X-WP-Total");
                    const data = response.body;
                    this.pagedResponse = {
                        data: data,
                        total: parseInt(total, 10),
                        total_pages: parseInt(total_pages, 10),
                    };
                    this.pagedResponse.data.forEach(post => {
                        post['readingTime'] = this.prefix_estimated_reading_time(post.content.rendered);
                    });
                    if (this.pagedResponse) {
                        observer.next(this.pagedResponse);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadPosts(limit, page: number = 1) {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        BlogService.ENDPOINT_POSTS + "?page=" + page + "&per_page=" + limit + "&lang=" + lang;
        return this.http.get<Response>(url, { observe: 'response' });
    }

    public getCategoryPosts(limit, page: number = 1, id): Observable<PagedResponse> {

        return new Observable<PagedResponse>(observer => {

            this.loadCategoryPosts(limit, page, id).subscribe(
                response => {
                    const total_pages = response.headers.get("X-WP-TotalPages");
                    const total = response.headers.get("X-WP-Total");
                    const data = response.body;
                    this.pagedResponse = {
                        data: data,
                        total: parseInt(total, 10),
                        total_pages: parseInt(total_pages, 10)
                    };
                    this.pagedResponse.data.forEach(post => {
                        post['readingTime'] = this.prefix_estimated_reading_time(post.content.rendered);
                    });
                    if (this.pagedResponse) {
                        observer.next(this.pagedResponse);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadCategoryPosts(limit, page: number = 1, id) {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        BlogService.ENDPOINT_POSTS + "?categories=" + id + "&page=" + page + "&per_page=" + limit + "&lang=" + lang;
        return this.http.get<Response>(url, { observe: 'response' });
    }

    public getRandPosts(limit, page: number = 1, currentPostId: number): Observable<Array<Blog>> {

        return new Observable<Array<Blog>>(observer => {

            this.loadRandPosts(limit, page, currentPostId).subscribe(
                response => {
                    // console.log(response);
                    if (response) {
                        this.posts = response;
                        this.posts.forEach(post => {
                            post['readingTime'] = this.prefix_estimated_reading_time(post.content.rendered);
                        });
                        observer.next(response);
                        observer.complete();
                    } else {
                        observer.error(response);
                    }
                },
                error => {
                    observer.error(error);
                }
            );
        });
    }

    loadRandPosts(limit, page: number = 1, currentPostId: number): Observable<Array<Blog>> {
        const lang = localStorage.getItem("lang");
        const url = Constants.getApiUrl() +
        BlogService.ENDPOINT_POSTS + '?per_page=' + limit + '&exclude=' + currentPostId + "&lang=" + lang;
        return this.http.get<Array<Blog>>(url);
    }

    getFeaturedImageUrl(image: FeaturedImage, isHome?: boolean) {
        if (!isHome) {
            if (image && image.media_details && image.media_details.sizes && image.media_details.sizes.blog_image) {
                return image.media_details.sizes.blog_image.source_url;
            } else {
                return Constants.DEFAULT_IMAGE;
            }
        } else {
            if (image && image.media_details && image.media_details.sizes && image.media_details.sizes.home_image) {
                return image.media_details.sizes.home_image.source_url;
            } else {
                return Constants.DEFAULT_IMAGE;
            }
        }
    }

    prefix_estimated_reading_time( content = '', $wpm = 300 ) {
        const clean_content = content.replace(/(<([^>]+)>)/ig, "");
        const word_count = clean_content.split(" ").length;
        const time = Math.ceil( word_count / $wpm );
        return time;
    }

}
