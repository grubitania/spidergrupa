import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageService } from 'src/app/services/page/page.service';
import { Blog } from 'src/app/interfaces/blog/blog';
import { Lightbox, LightboxConfig, LightboxEvent } from 'ngx-lightbox';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { ReplaySubject } from 'rxjs/internal/ReplaySubject';

@Component({
    selector: 'app-gallery-single',
    templateUrl: './gallery-single.component.html',
    styleUrls: ['./gallery-single.component.sass'],
    providers: [PageService]
})
export class GallerySingleComponent implements OnInit, OnDestroy {

    public static GALERIJA: string = "galerija";
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    private _album: Array<any> = [];
    post: Blog;
    images: Array<any> = [];
    title: string;

    constructor(
        private route: ActivatedRoute,
        private pageService: PageService,
        private _lightbox: Lightbox,
        private translateService: TranslateService
    ) {
        translateService.onLangChange.pipe(takeUntil(this.destroyed$)).subscribe((event: LangChangeEvent) => {
            this.route.params.subscribe(params => {
                this.loadImages(params['slug']);
            });
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.loadImages(params['slug']);
        });
    }

    loadImages(slug){
        this.pageService.getPage(GallerySingleComponent.GALERIJA).subscribe(response => {
            if (response) {
                this.post = response;
                if (response && response.acf && response.acf.gallery) {
                    response.acf.gallery.forEach(item => {
                        if (item.category_image.name === slug) {
                            this.images = item.images;
                            this.title = item.category_name;
                        }
                    });
                }
                this.images.forEach(item => {
                    this._album.push({'src': item.image.url});
                });
                console.log(this._album);
            } else {
                console.log("error while fetching the page " + slug);
            }
        });
    }

    open(index: number): void {
        // open lightbox
        this._lightbox.open(this._album, index);
    }

    close(): void {
        // close lightbox programmatically
        this._lightbox.close();
    }

    ngOnDestroy(){
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

}
