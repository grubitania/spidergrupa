import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Constants } from 'src/app/constants';
import { registerLocaleData } from '@angular/common';
import localeHr from '@angular/common/locales/hr';
import localeEn from '@angular/common/locales/en';
import { Router, NavigationEnd } from '@angular/router';


@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

    activeLanguage: string = localStorage.getItem("lang") ? localStorage.getItem("lang") : 'hr';
    navIsVisible: boolean = false;

    constructor(
        private translate: TranslateService,
        private router: Router
    ) { }

    ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            this.navIsVisible = false;
        });
    }

    public switchLang(lang: string, e?){
        if (e){
            e.preventDefault();
        }
        localStorage.setItem("lang", lang);
        this.activeLanguage = lang;
        if (lang === 'hr'){
            registerLocaleData(localeHr, 'hr');
        }else{
            registerLocaleData(localeEn, 'en');
        }
        this.translate.use(lang);
    }

    toggleNav(){
        this.navIsVisible = !this.navIsVisible;
    }
}
