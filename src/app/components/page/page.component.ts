import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Blog } from '../../interfaces/blog/blog';
import { PageService } from '../../services/page/page.service';
import { Title, Meta } from '@angular/platform-browser';

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.sass'],
    providers: [PageService],
    encapsulation: ViewEncapsulation.None
})
export class PageComponent implements OnInit, OnDestroy {
    public static POLITIKA_PRIVATNOSTI: string = "politika-privatnosti";
    public static UVJETI_KORISTENJA: string = "uvjeti-koristenja";
    pageID: number;
    post: Blog;

    constructor(
        private route: ActivatedRoute,
        private pageService: PageService,
        private meta: Meta,
        private title: Title
    ) { }

    ngOnInit() {
        this.route.params.subscribe(res => {
            if (res && res.slug === 'privacy-policy'){
                this.loadPage(PageComponent.POLITIKA_PRIVATNOSTI);
            }
            if (res && res.slug === 'terms-and-conditions'){
                this.loadPage(PageComponent.UVJETI_KORISTENJA);
            }
        });
    }

    loadPage(slug: string){
        this.pageService.getPage(slug).subscribe(response => {
            if (response){
                console.log(response);
                this.post = response;
                if (this.post && this.post.yoast_meta){
                    this.addMeta();
                }
            }else{
                console.log('error while fetching the page ' + slug);
            }
        });
    }

    addMeta(){
        this.title.setTitle(this.post.yoast_meta.yoast_wpseo_title);
        this.meta.addTag({ name: 'description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'keywords', content: this.post.yoast_meta.yoast_wpseo_focuskw });
        this.meta.addTag({ name: 'article:published_time', content: this.post.date });
        this.meta.addTag({ name: 'article:modified_time', content: this.post.modified });
        this.meta.addTag({ property: 'og:locale', content: 'hr' });
        this.meta.addTag({ property: 'og:type', content: 'article' });
        this.meta.addTag({ property: 'og:title', content: this.post.yoast_meta.yoast_wpseo_title });
        this.meta.addTag({ property: 'og:description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ property: 'og:url', content: window.location.href });
        this.meta.addTag({ property: 'og:site_name', content: 'spidergrupa.hr' });
        this.meta.addTag({ property: 'og:updated_time', content: this.post.modified });
        this.meta.addTag({ property: 'og:image', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'https://factory.hr/api/wp-content/uploads/2018/09/sharing.jpg' });
        this.meta.addTag({ property: 'og:image:secure_url', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'https://factory.hr/api/wp-content/uploads/2018/09/sharing.jpg' });
        this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
        this.meta.addTag({ name: 'twitter:title', content: this.post.yoast_meta.yoast_wpseo_twitter_title });
        this.meta.addTag({ name: 'twitter:description', content: this.post.yoast_meta.yoast_wpseo_twitter_description });
        this.meta.addTag({ name: 'twitter:image', content: this.post.yoast_meta.yoast_wpseo_twitter_image });
    }

    ngOnDestroy(){
        this.meta.removeTag("name='description'");
        this.meta.removeTag("name='keywords'");
        this.meta.removeTag("name='article:published_time'");
        this.meta.removeTag("name='article:modified_time'");
        this.meta.removeTag("property='og:locale'");
        this.meta.removeTag("property='og:type'");
        this.meta.removeTag("property='og:title'");
        this.meta.removeTag("property='og:description'");
        this.meta.removeTag("property='og:url'");
        this.meta.removeTag("property='og:site_name'");
        this.meta.removeTag("property='og:updated_time'");
        this.meta.removeTag("property='og:image'");
        this.meta.removeTag("property='og:image:secure_url'");
        this.meta.removeTag("name='twitter:card'");
        this.meta.removeTag("name='twitter:title'");
        this.meta.removeTag("name='twitter:description'");
        this.meta.removeTag("name='twitter:image'");
    }
}
