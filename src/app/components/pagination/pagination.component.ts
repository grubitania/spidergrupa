import { Component, OnInit, Input, ViewEncapsulation, OnChanges } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.sass'],
    encapsulation: ViewEncapsulation.None
})
export class PaginationComponent implements OnInit, OnChanges {

    @Input("totalPages") totalPages: number;
    @Input("paginationDataInput") paginationDataInput: any;

    delta: number = 3;
    range: Array<number> = [];
    rangeWithDots: Array<any> = [];

    nrOfPages: number;
    currentPage: number;
    l: number;
    separator: string = '...';

    paginationData: Array<{
        active: boolean,
        isLink: boolean,
        routerLink: string,
        text: string
    }> = [];

    constructor() { }

    ngOnInit() {
        this.paginationData = [];
        this.nrOfPages = this.totalPages;
        this.currentPage = parseInt(this.paginationDataInput.currentPage, 10);
        const range = this.generateRange();
        for (let i = 0; i < range.length; i++){
            const activeText = parseInt(range[i], 10) < 10 ? range[i] : range[i];
            if (range[i] !== "..."){
                this.paginationData.push({
                    active: parseInt(range[i], 10) == this.paginationDataInput.currentPage ? true : false,
                    isLink: parseInt(range[i], 10) != this.paginationDataInput.currentPage ? true : false,
                    routerLink: this.paginationDataInput.pageBase.replace("{page_num}", parseInt(range[i], 10)),
                    text: parseInt(range[i], 10) == this.paginationDataInput.currentPage ? parseInt(range[i], 10) : range[i]
                });
            }else{
                this.paginationData.push({
                    active: parseInt(range[i], 10) == this.paginationDataInput.currentPage ? true : false,
                    isLink: false,
                    routerLink: "",
                    text: range[i]
                });
            }
        }
    }

    ngOnChanges() {
        this.paginationData = [];
        this.nrOfPages = this.totalPages;
        this.currentPage = parseInt(this.paginationDataInput.currentPage, 10);
        const range = this.generateRange();
        for (let i = 0; i < range.length; i++){
            const activeText = parseInt(range[i], 10) < 10 ? range[i] : range[i];
            if (range[i] !== "..."){
                this.paginationData.push({
                    active: parseInt(range[i], 10) == this.paginationDataInput.currentPage ? true : false,
                    isLink: parseInt(range[i], 10) != this.paginationDataInput.currentPage ? true : false,
                    routerLink: this.paginationDataInput.pageBase.replace("{page_num}", parseInt(range[i], 10)),
                    text: parseInt(range[i], 10) == this.paginationDataInput.currentPage ? parseInt(range[i], 10) : range[i]
                });
            }else{
                this.paginationData.push({
                    active: parseInt(range[i], 10) == this.paginationDataInput.currentPage ? true : false,
                    isLink: false,
                    routerLink: "",
                    text: range[i]
                });
            }
            console.log(this.paginationData);
        }
    }

    generateRange() {
        this.range = [1];
        this.rangeWithDots = [];
        this.l = null;

        if (this.nrOfPages <= 1){
            return this.range;
        }

        for (let i = this.currentPage - this.delta; i <= this.currentPage + this.delta; i++) {
            if (i < this.nrOfPages && i > 1) {
                this.range.push(i);
            }
        }

        this.range.push(this.nrOfPages);

        for (const i of this.range) {
            if (this.l) {
                if (i - this.l == 2) {
                    this.rangeWithDots.push(this.l + 1);
                } else if (i - this.l != 1) {
                    this.rangeWithDots.push(this.separator);
                }
            }
            this.rangeWithDots.push(i);
            this.l = i;
        }

        return this.rangeWithDots;
    }

}
