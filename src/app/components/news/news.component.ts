import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogService } from '../../services/blog/blog.service';
import { Blog } from '../../interfaces/blog/blog';
import { Title, Meta } from '@angular/platform-browser';
import { trigger, transition, style, animate, query, stagger, keyframes } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';
import { FeaturedImage } from 'src/app/interfaces/blog/featuredImage';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { ReplaySubject } from 'rxjs/internal/ReplaySubject';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';

@Component({
      selector: 'app-news',
      templateUrl: './news.component.html',
      styleUrls: ['./news.component.sass'],
      providers: [BlogService],
      animations: [
        trigger('listAnimation', [
            transition('* => *', [

                query(':enter', style({ opacity: 0 }), {optional: true}),

                query(':enter', stagger('200ms', [
                    animate('0.3s ease', keyframes([
                        style({opacity: 0, transform: 'translateX(75%)', offset: 0}),
                        style({opacity: .5, transform: 'translateX(35px)',  offset: 0.5}),
                        style({opacity: 1, transform: 'translateX(0)',     offset: 1.0}),
                    ])
                )]), {optional: true})
            ])
        ])
    ]
})
export class NewsComponent implements OnInit, OnDestroy {

    public static PAGED_BASE = '/news/page/{page_num}';
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    posts: Array<Blog>;
    post: Blog;

    pageNum: number = 1;
    categorySlug: string;
    categoryMeta: string = "";

    postPerPage = 6;

    totalPages: number;
    totalPosts: number;
    paginationData: any;
    isCategory: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private blogService: BlogService,
        private title: Title,
        private meta: Meta,
        private translateService: TranslateService
    ) {
        translateService.onLangChange.pipe(takeUntil(this.destroyed$)).subscribe((event: LangChangeEvent) => {
            this.initLoading();
        });
    }

    ngOnInit() {
        this.initLoading();
    }

    initLoading(){
        this.route.params.subscribe(res => {
            if (res.pageNum) {
                this.pageNum = res.pageNum;
                this.loadPosts(this.pageNum);
            } else {
                this.postPerPage = 6;
                this.loadPosts(this.pageNum);
            }
            this.paginationData = {
                currentPage: this.pageNum,
                pageBase: NewsComponent.PAGED_BASE
            };
        });
    }

    loadPosts(page: number){
        this.blogService.getPosts(this.postPerPage, page).subscribe(
            response => {
                this.posts = response.data;
                this.totalPages = response.total_pages;
                this.totalPosts = response.total;
            },
            error => {
                console.log(error);
            }
        );
    }

    getFeaturedImageUrl(image: FeaturedImage){
        return this.blogService.getFeaturedImageUrl(image);
    }

    addMeta(){
        this.title.setTitle(this.post.yoast_meta.yoast_wpseo_title);
        this.meta.addTag({ name: 'description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'keywords', content: this.post.yoast_meta.yoast_wpseo_focuskw });
        this.meta.addTag({ name: 'article:published_time', content: this.post.date });
        this.meta.addTag({ name: 'article:modified_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:locale', content: 'hr' });
        this.meta.addTag({ name: 'og:type', content: 'article' });
        this.meta.addTag({ name: 'og:title', content: this.post.yoast_meta.yoast_wpseo_title });
        this.meta.addTag({ name: 'og:description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'og:url', content: window.location.href });
        this.meta.addTag({ name: 'og:site_name', content: 'spidergrupa.hr' });
        this.meta.addTag({ name: 'og:updated_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:image', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'https://factory.hr/api/wp-content/uploads/2018/09/sharing.jpg' });
        this.meta.addTag({ name: 'og:image:secure_url', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'https://factory.hr/api/wp-content/uploads/2018/09/sharing.jpg' });
        this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
        this.meta.addTag({ name: 'twitter:title', content: this.post.yoast_meta.yoast_wpseo_twitter_title });
        this.meta.addTag({ name: 'twitter:description', content: this.post.yoast_meta.yoast_wpseo_twitter_description });
        this.meta.addTag({ name: 'twitter:image', content: this.post.yoast_meta.yoast_wpseo_twitter_image });
    }

    ngOnDestroy(){
        this.meta.removeTag("name='description'");
        this.meta.removeTag("name='keywords'");
        this.meta.removeTag("name='article:published_time'");
        this.meta.removeTag("name='article:modified_time'");
        this.meta.removeTag("name='og:locale'");
        this.meta.removeTag("name='og:type'");
        this.meta.removeTag("name='og:title'");
        this.meta.removeTag("name='og:description'");
        this.meta.removeTag("name='og:url'");
        this.meta.removeTag("name='og:site_name'");
        this.meta.removeTag("name='og:updated_time'");
        this.meta.removeTag("name='og:image'");
        this.meta.removeTag("name='og:image:secure_url'");
        this.meta.removeTag("name='twitter:card'");
        this.meta.removeTag("name='twitter:title'");
        this.meta.removeTag("name='twitter:description'");
        this.meta.removeTag("name='twitter:image'");

        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

}
