import { Component, OnInit, OnDestroy, ElementRef, Input, ViewChild } from '@angular/core';
import { PageService } from 'src/app/services/page/page.service';
import { Blog } from 'src/app/interfaces/blog/blog';
import { Meta, Title } from '@angular/platform-browser';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { ReplaySubject } from 'rxjs/internal/ReplaySubject';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.sass'],
    providers: [PageService]
})
export class AboutComponent implements OnInit, OnDestroy {
    @ViewChild('slickModal') slickModal;
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    slideConfig = {
        "slidesToShow": 1,
        "slidesToScroll": 1,
        "arrows": false
    };
    post: Blog;
    company_history: Array<any>;
    slideIndex: number = 0;
    ready: boolean = false;

    constructor(
        private pageService: PageService,
        private meta: Meta,
        private title: Title,
        private translateService: TranslateService
    ) {
        translateService.onLangChange.pipe(takeUntil(this.destroyed$)).subscribe((event: LangChangeEvent) => {
            this.loadPage("o-kompaniji");
            this.loadAcf("povijest");
        });
    }

    ngOnInit() {
        this.loadPage("o-kompaniji");
        this.loadAcf("povijest");
    }


    loadPage(slug: string) {
        this.pageService.getPage(slug).subscribe(response => {
            if (response) {
                this.post = response;
                this.addMeta();
            } else {
                console.log("error while fetching the page " + slug);
            }
        });
    }

    loadAcf(slug: string) {
        this.pageService.getPage(slug).subscribe(response => {
            if (response) {
                if (response && response.acf && response.acf.company_history){
                    this.company_history = response.acf.company_history;
                    if (this.slickModal){
                        this.slickModal.unslick();
                    }
                    this.ready = true;
                }
            } else {
                console.log("error while fetching the page " + slug);
            }
        });
    }

    addMeta() {
        this.title.setTitle(this.post.yoast_meta.yoast_wpseo_title);
        this.meta.addTag({ name: 'description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'keywords', content: this.post.yoast_meta.yoast_wpseo_focuskw });
        this.meta.addTag({ name: 'article:published_time', content: this.post.date });
        this.meta.addTag({ name: 'article:modified_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:locale', content: 'hr' });
        this.meta.addTag({ name: 'og:type', content: 'article' });
        this.meta.addTag({ name: 'og:title', content: this.post.yoast_meta.yoast_wpseo_title });
        this.meta.addTag({ name: 'og:description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'og:url', content: window.location.href });
        this.meta.addTag({ name: 'og:site_name', content: 'spidergrupa.hr' });
        this.meta.addTag({ name: 'og:updated_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:image', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'http://spider.devtvornica.org/api/wp-content/uploads/2018/11/default-share-2.jpg' });
        this.meta.addTag({ name: 'og:image:secure_url', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'http://spider.devtvornica.org/api/wp-content/uploads/2018/11/default-share-2.jpg' });
        this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
        this.meta.addTag({ name: 'twitter:title', content: this.post.yoast_meta.yoast_wpseo_twitter_title });
        this.meta.addTag({ name: 'twitter:description', content: this.post.yoast_meta.yoast_wpseo_twitter_description });
        this.meta.addTag({ name: 'twitter:image', content: this.post.yoast_meta.yoast_wpseo_twitter_image });
    }

    ngOnDestroy() {
        this.meta.removeTag("name='description'");
        this.meta.removeTag("name='keywords'");
        this.meta.removeTag("name='article:published_time'");
        this.meta.removeTag("name='article:modified_time'");
        this.meta.removeTag("name='og:locale'");
        this.meta.removeTag("name='og:type'");
        this.meta.removeTag("name='og:title'");
        this.meta.removeTag("name='og:description'");
        this.meta.removeTag("name='og:url'");
        this.meta.removeTag("name='og:site_name'");
        this.meta.removeTag("name='og:updated_time'");
        this.meta.removeTag("name='og:image'");
        this.meta.removeTag("name='og:image:secure_url'");
        this.meta.removeTag("name='twitter:card'");
        this.meta.removeTag("name='twitter:title'");
        this.meta.removeTag("name='twitter:description'");
        this.meta.removeTag("name='twitter:image'");
        this.slickModal.unslick();
        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    slickUpdate(e){
        this.slideIndex = e.currentSlide;
        console.log(this.company_history);
    }

    goToSlide(slide: number){
        this.slideIndex = slide;
        console.log(this.company_history[this.slideIndex].content);
    }

    moveSlideNext(){
        this.slickModal.slickNext();
    }
    moveSlidePrev(){
        this.slickModal.slickPrev();
    }



}
