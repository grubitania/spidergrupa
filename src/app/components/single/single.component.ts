import { Component, OnInit, OnDestroy } from '@angular/core';
import { Blog } from 'src/app/interfaces/blog/blog';
import { Meta, Title } from '@angular/platform-browser';
import { BlogService } from 'src/app/services/blog/blog.service';
import { ActivatedRoute } from '@angular/router';
import { ReplaySubject } from 'rxjs/internal/ReplaySubject';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';

@Component({
  selector: 'app-single',
  templateUrl: './single.component.html',
  styleUrls: ['./single.component.sass'],
  providers: [BlogService]
})
export class SingleComponent implements OnInit, OnDestroy {
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    post: Blog;

    constructor(
        private route: ActivatedRoute,
        private blogService: BlogService,
        private meta: Meta,
        private title: Title,
        private translateService: TranslateService
    ) {
        translateService.onLangChange.pipe(takeUntil(this.destroyed$)).subscribe((event: LangChangeEvent) => {
            this.route.params.subscribe(params => {
                this.loadPost(params['slug']);
            });
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.loadPost(params['slug']);
        });
    }

    loadPost(slug){
        this.blogService.getPost(slug).subscribe(
            response => {
                this.post = response;
            },
            error => {
                console.log(error);
            }
        );
    }

    addMeta(){
        this.title.setTitle(this.post.yoast_meta.yoast_wpseo_title);
        this.meta.addTag({ name: 'description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'keywords', content: this.post.yoast_meta.yoast_wpseo_focuskw });
        this.meta.addTag({ name: 'article:published_time', content: this.post.date });
        this.meta.addTag({ name: 'article:modified_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:locale', content: 'hr' });
        this.meta.addTag({ name: 'og:type', content: 'article' });
        this.meta.addTag({ name: 'og:title', content: this.post.yoast_meta.yoast_wpseo_title });
        this.meta.addTag({ name: 'og:description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'og:url', content: window.location.href });
        this.meta.addTag({ name: 'og:site_name', content: 'spidergrupa.hr' });
        this.meta.addTag({ name: 'og:updated_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:image', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'https://factory.hr/api/wp-content/uploads/2018/09/sharing.jpg' });
        this.meta.addTag({ name: 'og:image:secure_url', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'https://factory.hr/api/wp-content/uploads/2018/09/sharing.jpg' });
        this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
        this.meta.addTag({ name: 'twitter:title', content: this.post.yoast_meta.yoast_wpseo_twitter_title });
        this.meta.addTag({ name: 'twitter:description', content: this.post.yoast_meta.yoast_wpseo_twitter_description });
        this.meta.addTag({ name: 'twitter:image', content: this.post.yoast_meta.yoast_wpseo_twitter_image });
    }

    ngOnDestroy(){
        this.meta.removeTag("name='description'");
        this.meta.removeTag("name='keywords'");
        this.meta.removeTag("name='article:published_time'");
        this.meta.removeTag("name='article:modified_time'");
        this.meta.removeTag("name='og:locale'");
        this.meta.removeTag("name='og:type'");
        this.meta.removeTag("name='og:title'");
        this.meta.removeTag("name='og:description'");
        this.meta.removeTag("name='og:url'");
        this.meta.removeTag("name='og:site_name'");
        this.meta.removeTag("name='og:updated_time'");
        this.meta.removeTag("name='og:image'");
        this.meta.removeTag("name='og:image:secure_url'");
        this.meta.removeTag("name='twitter:card'");
        this.meta.removeTag("name='twitter:title'");
        this.meta.removeTag("name='twitter:description'");
        this.meta.removeTag("name='twitter:image'");

        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

}
