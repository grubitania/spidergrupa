import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageService } from 'src/app/services/page/page.service';
import { Title, Meta } from '@angular/platform-browser';
import { Blog } from 'src/app/interfaces/blog/blog';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ContactService } from 'src/app/services/contact/contact.service';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { ReplaySubject } from 'rxjs/internal/ReplaySubject';


@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.sass'],
    providers: [PageService, ContactService]
})
export class ContactComponent implements OnInit, OnDestroy {
    public static KONTAKT: string = "kontakt";
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    post: Blog;
    contactForm: FormGroup;
    shouldDisplayThankYou: boolean = false;

    constructor(
        private pageService: PageService,
        private meta: Meta,
        private title: Title,
        private contactService: ContactService,
        private fb: FormBuilder,
        private translateService: TranslateService
    ) {
        translateService.onLangChange.pipe(takeUntil(this.destroyed$)).subscribe((event: LangChangeEvent) => {
            this.loadPage(ContactComponent.KONTAKT);
        });
    }

    ngOnInit() {
        this.loadPage(ContactComponent.KONTAKT);
        this.setUpForms();
    }

    loadPage(slug: string) {
        this.pageService.getPage(slug).subscribe(response => {
            if (response) {
                this.post = response;
                console.log(this.post);
                this.addMeta();
            } else {
                console.log("error while fetching the page " + slug);
            }
        });
    }

    addMeta() {
        this.title.setTitle(this.post.yoast_meta.yoast_wpseo_title);
        this.meta.addTag({ name: 'description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'keywords', content: this.post.yoast_meta.yoast_wpseo_focuskw });
        this.meta.addTag({ name: 'article:published_time', content: this.post.date });
        this.meta.addTag({ name: 'article:modified_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:locale', content: 'hr' });
        this.meta.addTag({ name: 'og:type', content: 'article' });
        this.meta.addTag({ name: 'og:title', content: this.post.yoast_meta.yoast_wpseo_title });
        this.meta.addTag({ name: 'og:description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'og:url', content: window.location.href });
        this.meta.addTag({ name: 'og:site_name', content: 'spidergrupa.hr' });
        this.meta.addTag({ name: 'og:updated_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:image', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'http://spider.devtvornica.org/api/wp-content/uploads/2018/11/default-share-2.jpg' });
        this.meta.addTag({ name: 'og:image:secure_url', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'http://spider.devtvornica.org/api/wp-content/uploads/2018/11/default-share-2.jpg' });
        this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
        this.meta.addTag({ name: 'twitter:title', content: this.post.yoast_meta.yoast_wpseo_twitter_title });
        this.meta.addTag({ name: 'twitter:description', content: this.post.yoast_meta.yoast_wpseo_twitter_description });
        this.meta.addTag({ name: 'twitter:image', content: this.post.yoast_meta.yoast_wpseo_twitter_image });
    }

    ngOnDestroy() {
        this.meta.removeTag("name='description'");
        this.meta.removeTag("name='keywords'");
        this.meta.removeTag("name='article:published_time'");
        this.meta.removeTag("name='article:modified_time'");
        this.meta.removeTag("name='og:locale'");
        this.meta.removeTag("name='og:type'");
        this.meta.removeTag("name='og:title'");
        this.meta.removeTag("name='og:description'");
        this.meta.removeTag("name='og:url'");
        this.meta.removeTag("name='og:site_name'");
        this.meta.removeTag("name='og:updated_time'");
        this.meta.removeTag("name='og:image'");
        this.meta.removeTag("name='og:image:secure_url'");
        this.meta.removeTag("name='twitter:card'");
        this.meta.removeTag("name='twitter:title'");
        this.meta.removeTag("name='twitter:description'");
        this.meta.removeTag("name='twitter:image'");

        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    setUpForms(): void{
        const email_regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        this.contactForm = this.fb.group({
            contact_name: new FormControl('', Validators.compose([
                Validators.required
            ])),
            contact_email: new FormControl('', Validators.compose([
                Validators.required,
                Validators.pattern(email_regex)
            ])),
            contact_message: new FormControl('', Validators.compose([
                Validators.required
            ])),
            gdpr: new FormControl('', Validators.required)
        });
    }

    validateAndSendContactForm(){
        const req = this.contactService.sendContact(this.contactForm.value);
        req.subscribe(
            response => {
                this.shouldDisplayThankYou = true;
            },
            error => {
                console.log(error);
            }
        );
        req.connect();
    }

}
