import { Component, OnInit, OnDestroy } from '@angular/core';
import { BlogService } from 'src/app/services/blog/blog.service';
import { Blog } from 'src/app/interfaces/blog/blog';
import { FeaturedImage } from 'src/app/interfaces/blog/featuredImage';
import { PageService } from 'src/app/services/page/page.service';
import { Meta, Title } from '@angular/platform-browser';
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { ReplaySubject } from 'rxjs/internal/ReplaySubject';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.sass'],
    providers: [BlogService, PageService]
})
export class HomeComponent implements OnInit, OnDestroy {

    public static SPIDER: string = "spider-grupa";
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    posts: Array<Blog>;
    post: Blog;
    showVideo: boolean = false;

    constructor(
        private blogService: BlogService,
        private pageService: PageService,
        private meta: Meta,
        private title: Title,
        private translateService: TranslateService
    ) {
        translateService.onLangChange.pipe(takeUntil(this.destroyed$)).subscribe((event: LangChangeEvent) => {
            this.loadPosts();
            this.loadPage(HomeComponent.SPIDER);
        });
    }

    ngOnInit() {
        this.loadPosts();
        this.loadPage(HomeComponent.SPIDER);
    }

    loadPosts() {
        this.blogService.getPosts(4, 1).subscribe(
            response => {
                this.posts = response.data;
            },
            error => {
                console.log(error);
            }
        );
    }

    loadPage(slug: string) {
        this.pageService.getPage(slug).subscribe(response => {
            if (response) {
                this.post = response;
                console.log(this.post);
                this.addMeta();
            } else {
                console.log("error while fetching the page " + slug);
            }
        });
    }

    addMeta() {
        this.title.setTitle(this.post.yoast_meta.yoast_wpseo_title);
        this.meta.addTag({ name: 'description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'keywords', content: this.post.yoast_meta.yoast_wpseo_focuskw });
        this.meta.addTag({ name: 'article:published_time', content: this.post.date });
        this.meta.addTag({ name: 'article:modified_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:locale', content: 'hr' });
        this.meta.addTag({ name: 'og:type', content: 'article' });
        this.meta.addTag({ name: 'og:title', content: this.post.yoast_meta.yoast_wpseo_title });
        this.meta.addTag({ name: 'og:description', content: this.post.yoast_meta.yoast_wpseo_metadesc });
        this.meta.addTag({ name: 'og:url', content: window.location.href });
        this.meta.addTag({ name: 'og:site_name', content: 'spidergrupa.hr' });
        this.meta.addTag({ name: 'og:updated_time', content: this.post.modified });
        this.meta.addTag({ name: 'og:image', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'http://spider.devtvornica.org/api/wp-content/uploads/2018/11/default-share-2.jpg' });
        this.meta.addTag({ name: 'og:image:secure_url', content: this.post.better_featured_image ? this.post.better_featured_image.source_url : 'http://spider.devtvornica.org/api/wp-content/uploads/2018/11/default-share-2.jpg' });
        this.meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
        this.meta.addTag({ name: 'twitter:title', content: this.post.yoast_meta.yoast_wpseo_twitter_title });
        this.meta.addTag({ name: 'twitter:description', content: this.post.yoast_meta.yoast_wpseo_twitter_description });
        this.meta.addTag({ name: 'twitter:image', content: this.post.yoast_meta.yoast_wpseo_twitter_image });
    }

    ngOnDestroy() {
        this.meta.removeTag("name='description'");
        this.meta.removeTag("name='keywords'");
        this.meta.removeTag("name='article:published_time'");
        this.meta.removeTag("name='article:modified_time'");
        this.meta.removeTag("name='og:locale'");
        this.meta.removeTag("name='og:type'");
        this.meta.removeTag("name='og:title'");
        this.meta.removeTag("name='og:description'");
        this.meta.removeTag("name='og:url'");
        this.meta.removeTag("name='og:site_name'");
        this.meta.removeTag("name='og:updated_time'");
        this.meta.removeTag("name='og:image'");
        this.meta.removeTag("name='og:image:secure_url'");
        this.meta.removeTag("name='twitter:card'");
        this.meta.removeTag("name='twitter:title'");
        this.meta.removeTag("name='twitter:description'");
        this.meta.removeTag("name='twitter:image'");

        this.destroyed$.next(true);
        this.destroyed$.complete();
    }

    getFeaturedImageUrl(image: FeaturedImage) {
        return this.blogService.getFeaturedImageUrl(image, true);
    }

    playVideo(e){
        e.preventDefault();
        this.showVideo = true;
    }
    stopVideo(e){
        e.preventDefault();
        this.showVideo = false;
    }

}
